
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <MeshRepairPlugin.hh>
#include <QString>
#include <QChar>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(MeshRepair, m) {

  QObject* pluginPointer = getPluginPointer("MeshRepair");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-MeshRepair" << std::endl;
     return;
   }

  MeshRepairPlugin* plugin = qobject_cast<MeshRepairPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-MeshRepair" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< MeshRepairPlugin,std::unique_ptr<MeshRepairPlugin, py::nodelete> > repair(m, "MeshRepair");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  repair.def(py::init([plugin]() { return plugin; }));

  repair.def("detectFlatValence3Vertices", &MeshRepairPlugin::detectFlatValence3Vertices,
                                 QCoreApplication::translate("PythonDocMeshRepair","Selects all vertices that have valence 3 and the normals of their neighboring faces have an angle less then the given angle").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","Maximal angle between the adjacent faces").toLatin1().data())  );

  repair.def("removeSelectedVal3Vertices", &MeshRepairPlugin::removeSelectedVal3Vertices,
                                 QCoreApplication::translate("PythonDocMeshRepair","Remove all selected valence 3 vertices").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );


  repair.def("selectEdgesShorterThan", &MeshRepairPlugin::selectEdgesShorterThan,
                                 QCoreApplication::translate("PythonDocMeshRepair","Selects all edges of an object which are shorter than the given length").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","All edges shorter than this length will be selected").toLatin1().data()) );

  repair.def("selectEdgesLongerThan", &MeshRepairPlugin::selectEdgesLongerThan,
                                 QCoreApplication::translate("PythonDocMeshRepair","Selects all edges of an object which are longer than the given length").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","All edges longer than this length will be selected").toLatin1().data()) );

  repair.def("removeSelectedEdges", &MeshRepairPlugin::removeSelectedEdges,
                                 QCoreApplication::translate("PythonDocMeshRepair","Remove the selected edges").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("detectSkinnyTriangleByAngle", &MeshRepairPlugin::detectSkinnyTriangleByAngle,
                                  QCoreApplication::translate("PythonDocMeshRepair","Select or remove skinny triangles (determined by a minimum angle threshold).").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","Minimum angle threshold").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","Remove? (Otherwise they will be selected)").toLatin1().data()));

  repair.def("detectFoldover", &MeshRepairPlugin::detectFoldover,
                                  QCoreApplication::translate("PythonDocMeshRepair","Selects edges that are incident to folded over faces.").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","Minimum threshold angle for fold-overs").toLatin1().data()) );


  repair.def("detectTriangleAspect", &MeshRepairPlugin::detectTriangleAspect,
                                  QCoreApplication::translate("PythonDocMeshRepair","Selects all faces that have a larger aspect ratio than the given one.").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocMeshRepair","The minimal aspect ratio to select").toLatin1().data()) );

  repair.def("flipOrientation", static_cast<void (MeshRepairPlugin::*) (int)>(&MeshRepairPlugin::flipOrientation),
                                 QCoreApplication::translate("PythonDocMeshRepair","Flips the normals of all faces by changing the vertex order in each face").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );


  repair.def("updateFaceNormals", &MeshRepairPlugin::updateFaceNormals,
                                 QCoreApplication::translate("PythonDocMeshRepair","Recompute Face normals").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("updateHalfedgeNormals", &MeshRepairPlugin::updateHalfedgeNormals,
                                 QCoreApplication::translate("PythonDocMeshRepair","Recompute Halfedge normals").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("updateVertexNormals", &MeshRepairPlugin::updateVertexNormals,
                                 QCoreApplication::translate("PythonDocMeshRepair","Recompute Vertex normals").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("updateNormals", &MeshRepairPlugin::updateNormals,
                                 QCoreApplication::translate("PythonDocMeshRepair","Recompute Face and Vertex normals").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("snapBoundary", &MeshRepairPlugin::snapBoundary,
                                 QCoreApplication::translate("PythonDocMeshRepair","Snaps selected boundary vertices together if they are closer than the given distance. No new vertices will be introduced on either edge, so they are just snapped to existing ones.").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","Max snapping distance").toLatin1().data()) );

  repair.def("fixNonManifoldVertices", &MeshRepairPlugin::fixNonManifoldVertices,
                                 QCoreApplication::translate("PythonDocMeshRepair","Remove non-manifold vertices by duplicating them").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()) );

  repair.def("fixMesh", &MeshRepairPlugin::fixMesh,
                                 QCoreApplication::translate("PythonDocMeshRepair","Fix Mesh: Degenerated faces will be removed and all vertices which are closer than the given distance will be collapsed. Non-manifold configurations at vertices will be removed and all faces of each component will be updated to have the same orientation.").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","ID of the object").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocMeshRepair","Max snapping distance").toLatin1().data()) );

}

